import os
import selenium
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
import time
import glob
import random
import uuid

VOWELS = 'aeiouy'
CONSONANTS = 'bcdfgmnpqrstvwz'

def get_random_word(length=6):
    """Produces a random word"""
    # We initialize the random number generator in a safe way, to guarantee random sequences
    # even in the cloud on cloned hosts.
    r = random.Random(str(uuid.uuid1()))
    s = ''
    for j in range(length / 2):
        s += r.choice(CONSONANTS) + r.choice(VOWELS)
    return s

def get_random_paragraph(num_words=50):
    """Generates a random paragraph."""
    r = random.Random(str(uuid.uuid1()))
    wlist = [get_random_word(length=r.choice([2, 4, 6, 8])) for _ in range(num_words)]
    return ' '.join(wlist)

def get_random_number(maxval=100):
    r = random.Random(str(uuid.uuid1()))
    return r.randint(0, maxval)

def get_random_title():
    return get_random_paragraph(num_words=4)

class Grading:

    def __init__(self, app_dir, test_url):
        self.app_dir = app_dir
        self.test_url = test_url
        self.grade = 0
        self.message_list = []
        self.browser = webdriver.Chrome()
        self._clear_database()
        self.browser.get(self.test_url)


    # This will delete all the database files in the database folder for the application
    def _clear_database(self):
        files = glob.glob(os.path.join(self.app_dir, "databases/*"))
        for f in files:
            os.remove(f)


    def _register_user(self, first, last, mail, passwrd):
        """
        Register a new user
        """
        try:
            self.browser.find_element_by_class_name("dropdown-toggle").click()  # Toggle drop-down for login
            register_link = self.browser.find_element_by_css_selector("a[href*='user/register?_next']")
            register_link.click()

            #Fill in the registration form
            first_name = self.browser.find_element_by_id("auth_user_first_name").send_keys(first)
            last_name = self.browser.find_element_by_id("auth_user_last_name").send_keys(last)
            email = self.browser.find_element_by_id("auth_user_email").send_keys(mail)
            password = self.browser.find_element_by_id("auth_user_password").send_keys(passwrd)
            confirm_password = self.browser.find_element_by_id("auth_user_password_two").send_keys(passwrd)
            submit_reg = self.browser.find_element_by_class_name("btn-primary").click()
            return (self.grade, "")
        except NoSuchElementException:
            return (self.grade, "Could not register a new user")


    def _login_user(self, email, passwrd):
        """
        Login the user based on the given email and password
        """
        try:
            self.browser.find_element_by_class_name("dropdown-toggle").click() #Toggle drop-down for login
            login_link = self.browser.find_element_by_css_selector("a[href*='user/login?_next']")
            login_link.click()

            #add stuff
            #Get the form fields
            username = self.browser.find_element_by_id('auth_user_email')
            password = self.browser.find_element_by_id('auth_user_password')
            submit_button = self.browser.find_element_by_class_name('btn-primary')

            #Fill in login form and submit
            username.send_keys(email)
            password.send_keys(passwrd)
            submit_button.click()
            return (self.grade, "")
        except NoSuchElementException:
            return (self.grade, "Could not login user")

    def _logout_user(self):
        """
        Navigate to the logout URL
        """
        self.browser.find_element_by_class_name("dropdown-toggle").click()  # Toggle drop-down for login
        logout_link = self.browser.find_element_by_css_selector("a[href*='user/logout?_next']")
        logout_link.click()


    def _add_post(self):
        """
        Add a single post to the page
        (requires post button to be 'Add new post')
        returns True if you can add a post and False if you canot
        """
        try:
            # Get the textarea to display
            add_new_post_button = self.browser.find_elements_by_xpath("//button[contains(text(), 'Add new post')]")
            add_new_post_button[0].click()

            # Add the text to the textarea (we know it is the first textarea on the page
            textarea = self.browser.find_elements_by_xpath("//textarea")
            textarea[0].send_keys("some text goes here")

            # Submit the post
            submit_post_button = self.browser.find_element_by_class_name("post-button")
            submit_post_button.click()

            return True
        except NoSuchElementException:
            return False


    def _user_can_post(self):
        """
        Checks whether or not the Add new post button is disabled
        Returns True if not disabled (user is logged in) and False if disable (user is not logged in)
        """
        add_post_button_off = self.browser.find_elements_by_xpath("//button[contains(text(), 'Add new post') "
                                                         "and contains(@class, 'disabled')]")
        if add_post_button_off:
            add_post_button_off[0].click()
            cancel_button = self.browser.find_elements_by_xpath("//button[contains(text(), 'Cancel')]")
            # If the cancel button is there then you can add a post, so return True
            if cancel_button:
                return True
            return False
        else:
            return True


    def _edit_post(self, n, text):
        """
        Edit the n-th post on the page that is yours
        (requires that fa-pencil is used for the edit button. And text is in p tag)
        returns True if you were able to edit a post and False if you cannot
        """

        try:
            # edit the first post you made
            edit_button = self.browser.find_element_by_xpath("//i[@class='fa-pencil'][%d]" % (n + 1))
            edit_button.click()

            # change the text
            textarea = self.browser.find_elements_by_xpath("//textarea")
            textarea[0].clear()
            textarea[0].send_keys(text)

            # submit the post
            submit_post_button = self.browser.find_element_by_class_name("post-edit-button")
            submit_post_button.click()

            time.sleep(1)

            # Check that the post has been modified
            edited_post = self.browser.find_elements_by_xpath("//div[@class='post-content'][%d]//p[contains(text(), '%s')]" %
                                                              (n + 1, text))
            return True
        except NoSuchElementException:
            return False


    def _delete_post(self):
        """
        Delete the first post on the page that is yours
        (requires that fa-trash-o is used for the delete icon)
        returns True if it deletes a post and false otherwise
        """
        try:
            delete_button = self.browser.find_element_by_class_name("fa-trash-o")
            delete_button.click()
            return True
        except NoSuchElementException:
            return False


    def _check_edit(self):
        """
        Check if the edit icon is available for another user (return true if it is not available)
        (requires that fa-pencil is used for the edit button)
        returns True if you cannot edit a post and False if you can
        """
        try:
            edit_button = self.browser.find_element_by_class_name("fa-pencil")
            return False
        except NoSuchElementException:
            return True


    def _check_has_more(self):
        """
        Check whether or not the load more button is showing
        (requires that the show_more class is used for the load more button)
        returns True is load more is showing and False if it is not
        """
        try:
            self.browser.find_element_by_class_name("show_more")
            return True
        except NoSuchElementException:
            return False

    def _click_has_more(self):
        """
        Clicks on the has_more button
        """
        try:
            has_more_buttom = self.browser.find_element_by_xpath("//div[contains(@class,'show_more')]//button")
            has_more_buttom.click()
            return True
        except NoSuchElementException:
            return False

    ####################################

    def add_posts_then_check(self):
        """
        Adds four new post to the database and then check whether or not load more is present
        If load more is present return True, if not return False
        """
        points = 0
        error_list = []
        self._add_post()
        time.sleep(1)
        self._add_post()
        time.sleep(1)
        self._add_post()
        time.sleep(1)
        self._add_post()
        time.sleep(1)
        self._add_post()
        time.sleep(1)

        self.browser.refresh()

        list_of_posts = self.browser.find_elements_by_xpath("//div[contains(@class, 'post')]") # should be 4
        if len(list_of_posts) != 4:
            return False

        self._click_has_more()
        time.sleep(1)

        list_of_posts_after = self.browser.find_elements_by_xpath("//div[contains(@class, 'post')]")  # should be 5

        return len(list_of_posts_after) == 5



    #####################
    # TEST FUNCTIONS    #
    #####################


    def register_two_users(self):
        """
        Cleans out the database and then registers two users
        """
        self._clear_database()
        time.sleep(1)
        self._register_user("john", "smith", "example@aol.com", "hello")
        time.sleep(1)
        self._logout_user()
        self._register_user("michael", "smith", "another@aol.com", "hello")
        time.sleep(1)
        self._logout_user()



    def add_edit_post_logged_in(self):
        """
        Tests that a logged in user can add a post
        If the user can post increment the grade
        Else print an error message
        """
        #add two users and log into one
        self.register_two_users()
        time.sleep(1)
        self._login_user("example@aol.com", "hello")
        time.sleep(1)

        #add and edit a post
        self._add_post()
        time.sleep(1)
        self._add_post()
        time.sleep(1)
        if self._add_post():
            self.grade += 1
            time.sleep(1)
            text = get_random_paragraph(10)
            n = random.randint(0, 2)
            if self._edit_post(n, text):
                self.grade += 1
            else:
                print "ERROR: user could not edit their own post"
        else:
            print "ERROR: user could not add post"

        self._logout_user()
        time.sleep(1)

    def add_post_logged_out(self):
        self._clear_database()
        time.sleep(1)
        if not self._user_can_post():
            self.grade += 1
        else:
            print "ERROR: able to post without being logged in"
        time.sleep(1)


    def cant_edit_other_user(self):
        self.register_two_users()
        time.sleep(1)

        self._login_user("example@aol.com", "hello")
        time.sleep(1)

        self._add_post()
        time.sleep(2)

        self._logout_user()
        time.sleep(1)

        self._login_user("another@aol.com", "hello")
        time.sleep(1)

        if self._check_edit():
            self.grade += 1
        else:
            print "ERROR: user is able to edit another user's post"

        self._logout_user()
        time.sleep(1)

    """
    Test that the load more button shows up once there are more than 5 posts on the page
    """
    def test_load_more(self):
        self.register_two_users()
        time.sleep(1)

        self._login_user("example@aol.com", "hello")
        time.sleep(1)

        if not self._check_has_more():
            self.grade += 1 #load more should not show until 5 or more posts
        else:
            print "ERROR: load more shows with less than 5 posts in database"

        if self.add_posts_then_check():
            self.grade += 1 # Add 5 posts the check that load more shows
        else:
            print "ERROR: load more is not displayed after adding 5 posts"

        self._logout_user()
        time.sleep(1)


    """
    This function will add a post and then delete the post
    If the post deletes then add one to grade
    """
    def test_delete_post(self):
        self.register_two_users()
        time.sleep(1)

        self._login_user("example@aol.com", "hello")
        time.sleep(1)

        self._add_post()
        time.sleep(1)

        # Make sure post added
        try:
            check_divs = self.browser.find_element_by_class_name("post")
        except NoSuchElementException:
            print "ERROR: could not add post"

        # Delete the post that was added
        self._delete_post()
        time.sleep(1)
        # Make sure the post deleted
        try:
            check_divs = self.browser.find_element_by_class_name("post")
            print "ERROR: use could not delete a post"
        except NoSuchElementException:
            self.grade += 1


    """
    Run the tests and increment grades/print errors where appropriate
    """
    def run_grading(self):
        self.add_post_logged_out()
        self.add_edit_post_logged_in()
        self.cant_edit_other_user()
        self.test_load_more()
        self.test_delete_post()

        # Print the grade
        print "YOU GOT A: " + str(self.grade)




import sys

#Path to app, URL
grade1 = Grading(sys.argv[1], sys.argv[2])
grade1.run_grading()
